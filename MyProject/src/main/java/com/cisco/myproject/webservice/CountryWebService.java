package com.cisco.myproject.webservice;

import com.cisco.myproject.model.Country;

public interface CountryWebService extends WebService<Country> {

}
