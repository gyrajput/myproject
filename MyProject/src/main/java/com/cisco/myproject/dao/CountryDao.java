package com.cisco.myproject.dao;

import com.cisco.myproject.model.Country;

public interface CountryDao extends Dao<Country> {

}
