package com.cisco.myproject.mapper;

import com.cisco.myproject.model.Country;

public interface CountryMapper extends Mapper<Country> {

}
